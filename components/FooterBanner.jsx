import React from 'react'

import Link from 'next/link'
import { urlFor } from '../lib/client'

const FooterBanner = ({footerBanner: { discount,
largeText1, largeText2, saleTime, smallText, 
midText, product, buttonText, image, desc } }) => {
  return (
    <div className="footer-banner-container">
      <div className="banner-desc">
        <div className="left">
          <p style={{marginLeft: '30px'}}>{discount}</p>
          <h3>{largeText1}</h3>
          <p style={{marginLeft: '30px'}}>c {saleTime}</p>
        </div>
        <div className="right">
          <h3>{midText}</h3>
          <p>{desc}</p>
          <Link href={`/product/${product}`}>
            <button type="button">{buttonText}</button>
          </Link>
        </div>
        <img 
        src={urlFor(image)}
        className="footer-banner-image"
        alt="наушники"
        />
      </div>
    </div>
  )
}

export default FooterBanner