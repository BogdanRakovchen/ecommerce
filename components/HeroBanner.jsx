import React from 'react'
import Link from 'next/link'

import { urlFor } from '../lib/client'

const HeroBanner = ({heroBanner}) => {
  const {image, buttonText, product, 
  desc, smallText, midText, largeText1,
largeText2, discount, saleTime } = heroBanner
const headPhones = product.replace('наушники', 'headphones')
  return (
    <div className="hero-banner-container">
      <div>
        {/* <p className="beats-solo">{smallText}</p> */}
        <h3>{midText}</h3>
        <br />
        <h1>{largeText1}</h1>
        <img src={urlFor(image)} alt="headphones" 
        className="hero-banner-image"/>
        <div>
          <Link href={`/product/${headPhones}`}>
            <button type="button">{buttonText}</button>
          </Link>
          <div className="desc">
            <h5>Описание</h5>
            <p>{desc}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default HeroBanner