import React, {useState, useEffect} from 'react'
import Link from 'next/link'
import {BsBagCheckFill} from 'react-icons/bs'
import { useStateContenxt } from '../Context/StateContext'
import { runFireworks } from '../lib/utils'
const Success = () => {

    const {setCartItems, setTotalPrice, 
    setTotalQuantities} = useStateContenxt()
    

useEffect(() => {
  runFireworks(),
  localStorage.clear(),
  setCartItems([]), 
  setTotalPrice(0), 
  setTotalQuantities(0)
}, [])


  return (
    <div className="success-wrapper">
        <div className="success">
            <p className="icon">
                <BsBagCheckFill />
            </p>
            <h2>Спасибо за Ваш заказ!</h2>
            <p className="email-msg">
               Укажите свой email для получения чека
            </p>
            <p className="description">
               Если у Вас есть вопросы, отправьте нам
               <a className="email"
                  href="mailto:order@example.com">
                 order@example.com
               </a>
            </p>
            <Link href="/">
              <button 
                type="button"
                width="300px"
                className="btn" 
              >
                Продолжить покупки
              </button>
            </Link>
        </div>
    </div>
  )
}

export default Success