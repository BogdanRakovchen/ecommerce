import React, {useState} from 'react'
import {AiOutlineMinus, AiOutlinePlus,
AiFillStar, AiOutlineStar} from 'react-icons/ai'

import { client, urlFor } from '../../lib/client'

import Product from '../../components/Product'

import {useStateContenxt} from '../../Context/StateContext'


const ProductDetails = ({ product, products }) => {

    const { image, name, details, Price } = product
    const [index, setIndex] = useState(0)
    const {decQty, incQty, qty, onAdd, setShowCart} = useStateContenxt();
    
    const handleBuyNow = () => {
        onAdd(product, qty);

        setShowCart(true)
    }

    return (
    <div>
        <div className="product-detail-container">
            <div>
                <div className="image-container">
                    <img 
                    className="product-detail-image"
                    src={urlFor(image && image[index])}
                    />
                </div>
                <div className="small-images-container">
                    {image?.map((item, i) => (
                    <img 
                    key={i}
                    src={urlFor(item)}
                    className={i === index ? 'small-image selected-image' 
                    : 'small-image'}
                    onMouseEnter={() => setIndex(i)}
                    />    
                    ))}
                </div>
            </div>
            <div className="product-detail-desc">
                <h1>{name}</h1>
                <div className="reviews">
                    <div>
                    <AiFillStar />
                    <AiFillStar />
                    <AiFillStar />
                    <AiFillStar />
                    <AiOutlineStar />
                    </div>
                    <p>
                        (20)
                    </p>
                </div>
                <h4>Details: </h4>
                <p>{details}</p>
                <p className="price">${Price}</p>
                <div className="quantity">
                    <h3>Quantity:</h3>
                    <p className="quantity-desc">
                        <span
                        onClick={decQty} 
                        className="minus">
                        <AiOutlineMinus />
                        </span>
                        <span
                        className="num">
                        {qty}
                        </span>
                        <span
                        onClick={incQty} 
                        className="plus">
                        <AiOutlinePlus />
                        </span>
                    </p>
                </div>
                <div className="buttons">
                    <button 
                    className="add-to-cart"
                    type="button"
                    onClick={() => onAdd(product, qty)}
                    >
                     Добавить товар   
                    </button>
                    <button 
                    className="buy-now"
                    type="button"
                    onClick={handleBuyNow}
                    >
                     Купить   
                    </button>
                </div>
            </div>
        </div>
        <div className="maylike-products-wrapper">
            <h2>Вам также может понравиться</h2>
            <div className="marquee">
                <div 
                className="maylike-products-container track">
                    {products.map((item) => (
                        <Product 
                        key={item._id}
                        product={item}
                        />
                    ))}
                </div>
            </div>
        </div>
    </div>
  )
}

export const getStaticPaths = async () => {
    const query = `*[_type == "product"] {
        slug {
            current
        }
    }` ;

    const products = await client.fetch(query)

    const paths = products.map((product) => ({
        params: {
            slug: product.slug.current
        }
    }));

    return {
        paths, 
        fallback: 'blocking'
    }
}


export const getStaticProps = async ({ params: {slug} }) => {
    const query = `*[_type == "product" && slug.current == '${slug}'][0]`
    const productsQuery = `*[_type == "product"]`
   
    const product = await client.fetch(query)
    const products = await client.fetch(productsQuery)
   
    return {
      props: {products, product}
    }
  }

export default ProductDetails