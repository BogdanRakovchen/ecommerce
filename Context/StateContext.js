import React, {createContext, useContext, 
useState, useEffect} from 'react'
import {toast} from 'react-hot-toast'
import getStripe from '../lib/getStripe'


const Context = createContext()

export const StateContext = ({ children }) => {
    const [showCart, setShowCart] = useState(false)
    const [cartItems, setCartItems] = useState([])
    const [totalPrice, setTotalPrice] = useState(0)
    const [totalQuantities, setTotalQuantities] = useState(0)
    const [qty, setQty] = useState(1)


    let foundProduct;
    let index;

    

    const onAdd = (product, quantity) => {
        const checkProductInCart = cartItems.find((item) => item._id === product._id)
        setTotalPrice((prevTotalPrice) => prevTotalPrice + product.Price * quantity)
        setTotalQuantities((prevTotalQuantity) => prevTotalQuantity + quantity)
       
        if(checkProductInCart) {
          const updateCartItems = cartItems.map((cartProduct) => {
            if(cartProduct._id === product._id) return {
                ...cartProduct,
                quantity: cartProduct.quantity + quantity
            }
        })
        setCartItems(updateCartItems)
        } else {
            product.quantity = quantity
            setCartItems([...cartItems, {...product}])
        }
        toast.success(`${qty} ${product.name} добавлено в корзину`)

    }

    const onRemove = (product) => {
        foundProduct = cartItems.find((item) => item._id === product._id)
        const newCartItems = cartItems.filter((item) => item._id !== product._id)

        setTotalPrice((prevTotalPrice) => (
            prevTotalPrice - foundProduct.Price * foundProduct.quantity
        ))

        setTotalQuantities((prevTotalQuantities) => (
            prevTotalQuantities - foundProduct.quantity
        ))
        setCartItems(newCartItems)
    }

    const toggleCartItemQuantity = (id, value) => {
        foundProduct = cartItems.find((item) => item._id === id)
        index = cartItems.findIndex((product) => product._id === id);
        const newCartItems = cartItems.filter((item) => item._id !== id)
    
        if(value === 'inc') {
          setCartItems([...newCartItems, { ...foundProduct, quantity: foundProduct.quantity + 1 } ]);
          setTotalPrice((prevTotalPrice) => prevTotalPrice + foundProduct.Price)
          setTotalQuantities((prevTotalQuantities) => prevTotalQuantities + 1)
          console.log(totalQuantities, 'inc');
        } else if(value === 'dec') {
          if (foundProduct.quantity > 1) {
            setCartItems([...newCartItems, { ...foundProduct, quantity: foundProduct.quantity - 1 } ]);
            setTotalPrice((prevTotalPrice) => prevTotalPrice - foundProduct.Price)
            setTotalQuantities((prevTotalQuantities) => prevTotalQuantities - 1)
            console.log(totalQuantities, 'dec');

          }
        }
      }

    const incQty = () => {
        setQty(prevQty => prevQty + 1)
    }

    const decQty = () => {
        setQty(prevQty => {
            if(prevQty - 1 < 1) return 1    
            return prevQty - 1
        }
            
        )

    }

    const handleCheckout = async () => {
        const stripe = await getStripe()
    
        const response = await fetch('/api/stripe', {
          method: 'POST',
          headers: {
            'Content-Type': "application/json",
          },
          body: JSON.stringify(cartItems),
    
        }).then(res => res.json()).then(data => {
          //toast.loading('Переадресация на сервис оплаты...');
           console.log(data, 'data'); 
          stripe.redirectToCheckout({ sessionId: data.id })
        })
      
      }

    return (
        <Context.Provider
        value={{
            showCart,
            cartItems,
            totalPrice,
            totalQuantities,
            qty,
            incQty,
            decQty,
            onAdd,
            setShowCart,
            toggleCartItemQuantity,
            onRemove,
            handleCheckout,
            setCartItems, 
            setTotalPrice, 
            setTotalQuantities
        }}   
        >
        {children}
        </Context.Provider>
    )
}

export const useStateContenxt = () => useContext(Context)